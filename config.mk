PREFIX = /usr
PKGCONFIGDIR = $(PREFIX)/lib/pkgconfig

CXX = c++ -std=c++11

CPPFLAGS =
CXXFLAGS = -Wall -O3
LDFLAGS = -lgamma
